# Demo Bluetooth y MIT App Inventor
Para el demo se utiliza la [**DWM1001** *DEVELOPMENT BOARD*](https://www.decawave.com/product/dwm1001-development-board/) de Decawave. El *firmware* se desarrolló sobre **SEGGER Embedded Studio** y utilizando el SDK de Nordic.

Esta placa contiene un módulo [DWM1001C](https://www.decawave.com/product/dwm1001-module/), el cual a su vez incorpora un [nRF52832](https://www.nordicsemi.com/Products/Low-power-short-range-wireless/nRF52832) que tiene *Bluetooth*.

En el archivo [`main.c`](nRF5/ble_app_uart/main.c) se encuentra la lógica que se pidió desarrollar.

La función [`void nus_data_handler(ble_nus_evt_t * p_evt)`](nRF5/ble_app_uart/main.c#L196) es llamada cuando llegan datos por *Bluetooth* y aquí se enciende y apaga el LED comandado por la [aplicación desarollada en App Inventor](MIT_App_Inventor/PracticoPdCSE.aia)

La función [`void bsp_event_handler(bsp_event_t event)`](nRF5/ble_app_uart/main.c#L485) es llamada cuando una GPIO cambia de estado y aquí se envían los datos a la [aplicación desarollada en App Inventor](MIT_App_Inventor/PracticoPdCSE.aia) para cambiar en estado del *label* que simula un LED.

# Video Demostrativo
![](docs/video/demo.mp4)
