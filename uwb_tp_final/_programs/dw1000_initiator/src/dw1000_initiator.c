/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/17
 *
 * Adaptado de: main_nRF_responder.c
 *===========================================================================*/
/*! ----------------------------------------------------------------------------
*  @file    main_nRF_responder.c
*  @brief   Single-sided two-way ranging (SS TWR) responder example code
*
*           This is a simple code example which acts as the responder in a SS TWR distance measurement exchange.
*           This application waits for a "poll" message (recording the RX time-stamp of the poll) expected from
*           the "SS TWR initiator" example code (companion to this application), and
*           then sends a "response" message recording its TX time-stamp,
*
* @attention
*
* Copyright 2018 (c) Decawave Ltd, Dublin, Ireland.
*
* All rights reserved.
*
* @author Decawave
*/
/*=====[Inclusions of function dependencies]=================================*/

#include "dw1000_initiator.h"
#include "dw1000.h"
#include "board.h"

#include "sdk_config.h"

/*=====[Definition macros of private constants]==============================*/
//-----------------dw1000----------------------------

static dwt_config_t config = { 5, 	/* Channel number. */
			DWT_PRF_64M, 			/* Pulse repetition frequency. */
			DWT_PLEN_128, 			/* Preamble length. Used in TX only. */
			DWT_PAC8, 				/* Preamble acquisition chunk size. Used in RX only. */
			10,						/* TX preamble code. Used in TX only. */
			10, 					/* RX preamble code. Used in RX only. */
			0, 						/* 0 to use standard SFD, 1 to use non-standard SFD. */
			DWT_BR_6M8, 			/* Data rate. */
			DWT_PHRMODE_STD,	 	/* PHY header mode. */
			(129 + 8 - 8) 			/* SFD timeout (preamble length + 1 + SFD length - PAC size). Used in RX only. */
};

/*=====[Definitions of extern global variables]==============================*/

extern double distance;
extern void ss_init_run();


/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Main function, program entry point after power on or reset]==========*/

/**@brief Application main function.
 */
int main(void)
{

    dw1000_init(config);

    //Start execution.
    printf("\r\nUART started.\r\n");

    // Enter main loop.
    for (;;)
    {
		ss_init_run();
		printf("%f", distance);
    }
}
